using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	public float Speed = 1;
	public Vector3 _targetPos;
	Vector3 _startPos;

	// Use this for initialization
	void Start () {
		_startPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	[ContextMenu("Open")]
	public void Open()
	{
		StartCoroutine (OpenDoor ());
	}

	IEnumerator OpenDoor()
	{
		var distance = Vector3.Distance (_startPos, _targetPos);
		float perc = 0;
		while (distance > 0.2f) {
			transform.position = Vector3.Lerp(_startPos,_targetPos,perc);
			perc += Time.deltaTime * Speed;
			yield return new WaitForEndOfFrame();
		}
	}
}
