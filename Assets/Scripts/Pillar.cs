﻿using UnityEngine;
using System.Collections;

public class Pillar : MonoBehaviour {

    public Civilization Civ;
    bool _activated;
    
    public void Activate()
    {
        if (!_activated)
        {
            _activated = true;
            GameEvents.PlayerActivatedPuzzle(Civ);
        }
    }

    public void Deactivate()
    {
        if (_activated)
        {
            _activated = false;
            GameEvents.PlayerDeactivatedPuzzle(Civ);
        }
    }
}
