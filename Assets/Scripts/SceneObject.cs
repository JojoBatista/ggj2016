﻿using UnityEngine;
using System.Collections;

public class SceneObject : MonoBehaviour {
    
    public Color[] ColorForCivs;
	Renderer _rend;

	void Awake()
	{
		_rend = GetComponent<Renderer> ();
	}

	void Start()
	{
		ChangeMaterial (Player.CivType);
	}


	void ChangeMaterial (Civilization civ)
	{
        _rend.material.color = ColorForCivs [(int)civ];
	}
}
