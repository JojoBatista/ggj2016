﻿using UnityEngine;
using System.Collections;
using System;

public static class GameEvents {

    public static Action OnTokenRecovered;

    public static void TokenRecovered()
    {
        if (OnTokenRecovered != null)
            OnTokenRecovered();
    }

    public static Action<Civilization> OnPlayerActivatedPuzzle;

    public static void PlayerActivatedPuzzle(Civilization civ)
    {
        if (OnPlayerActivatedPuzzle != null)
            OnPlayerActivatedPuzzle(civ);
    }

    public static Action<Civilization> OnPlayerDeactivatedPuzzle;

    public static void PlayerDeactivatedPuzzle(Civilization civ)
    {
        if (OnPlayerDeactivatedPuzzle != null)
            OnPlayerDeactivatedPuzzle(civ);
    }

    public static Action<Objective> OnInstantiateGameplayObjects;

    public static void InstantiateGameplayObjects(Objective objective)
    {
        if (OnInstantiateGameplayObjects != null)
            OnInstantiateGameplayObjects(objective);
    }

    public static Action OnGameWon;

    public static void GameWon()
    {
        if (OnGameWon != null)
            OnGameWon();
    }

	public static Action OnPuzzleCompleted;

	public static void PuzzleCompleted()
	{
		if (OnPuzzleCompleted != null)
			OnPuzzleCompleted ();
	}
}
