﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Puzzle {

    public int Activated;
    public Objective[] Objectives;
	public Door Door;
}

[System.Serializable]
public class Objective
{
    public Civilization Civ;
    public string GameObjectName;
    public Vector3 Position;
}