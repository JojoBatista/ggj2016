﻿using UnityEngine;
using System.Collections;

public enum Civilization
{
	Egypt,
	Viking,
	Mayan,
	Greek
}

public class Player : MonoBehaviour {

	public static Civilization CivType;
    public float MaxTokenDistance = 2f;
    GameObject _token;
    GameObject weapon = null; // Model used in First Person camera
    bool _tokenUsed, _tokenLocked;

    private Pillar activePillar = null;

	void Start()
	{
		CivType = (Civilization)PhotonNetwork.player.ID-1;
        
        Vector3 muzzleOffset = gameObject.transform.position + new Vector3(-1, -0.5f, 0.5f);
        weapon = Instantiate(Resources.Load("token_" + (int)CivType), muzzleOffset, Quaternion.identity) as GameObject;
        weapon.transform.parent = Camera.main.transform;
    }

    void Update()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        if (Input.GetMouseButtonDown(0))
        {
            PlaceToken();
        }
    }

    void OnEnable()
    {
        GameEvents.OnTokenRecovered += TokenRecovered;
        GameEvents.OnInstantiateGameplayObjects += InstantiateGameplayObjects;
		GameEvents.OnPuzzleCompleted += PuzzleCompleted;
    }

    void OnDisable()
    {
        GameEvents.OnTokenRecovered -= TokenRecovered;
        GameEvents.OnInstantiateGameplayObjects -= InstantiateGameplayObjects;
		GameEvents.OnPuzzleCompleted -= PuzzleCompleted;
    }

	void PuzzleCompleted ()
	{
		activePillar = null;
        _tokenLocked = false;
	}

    private void InstantiateGameplayObjects(Objective objective)
    {
        if (objective.Civ == CivType)
        {
            PhotonNetwork.Instantiate(objective.GameObjectName, objective.Position, Quaternion.identity, 0);
        }
    }

    private void TokenRecovered()
    {
        PhotonNetwork.Destroy(_token);
        _tokenUsed = false;
        weapon.SetActive(!_tokenUsed);
    }

	public void PlaceToken()
    {
        

		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit))
		{
			//check if the collition is valid
			if(hit.collider != null && Vector3.Distance(transform.position, hit.point ) < MaxTokenDistance)
			{
                if (hit.transform.CompareTag("Token"))
                    return;

                // If we hit a pillar...
                Pillar pillar = hit.transform.GetComponent<Pillar>();
                if (pillar != null)
                {
                    // if we already had an active pillar
                    if (activePillar != null && pillar != activePillar)
                    {
                        activePillar.Deactivate();
                    }
                    
                    // Set the new pillar as active, and activate it
                    if (pillar.Civ == CivType)
                    {
                        pillar.Activate();
                        _tokenLocked = true;
                    }
                }
                else if (activePillar != null)
                {
                    activePillar.Deactivate();
                }
                activePillar = pillar;
                

                if (_tokenUsed && _token != null)
                {
                    TokenRecovered();
                }

                _token = PhotonNetwork.Instantiate("token_" + (int)CivType, hit.point, Quaternion.identity, 0);
                _tokenUsed = true;
                weapon.SetActive(!_tokenUsed);
			}
		}        
    }
}
