﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PreGameScene : Photon.MonoBehaviour {

	Text t;
	// Use this for initialization
	void Start () {
		t = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable()
	{

	}

	public void OnJoinedRoom()
	{
		t.text = string.Format ("{0}/{1} Players", PhotonNetwork.room.playerCount, 4);

		if (PhotonNetwork.room.playerCount >= 4) {
			photonView.RPC("LoadScene",PhotonTargets.All, null);
		}
	}

	[PunRPC]
	void LoadScene()
	{
		PhotonNetwork.automaticallySyncScene = true;
		Application.LoadLevel ("Game");
	}
}
