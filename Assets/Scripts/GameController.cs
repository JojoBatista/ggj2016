﻿using UnityEngine;
using System.Collections;

public class GameController : Photon.MonoBehaviour {

    public Puzzle[] Puzzles;
    int _currentPuzzleID;
    Puzzle _currentPuzzle;

	// Use this for initialization
	void Start () {
		PhotonNetwork.isMessageQueueRunning = true;
	   /* if(PhotonNetwork.isMasterClient && Puzzles.Length > 0)
        {*/
            foreach(var puzzle in Puzzles)
            {
                foreach(var obj in puzzle.Objectives)
                {
                    //Send RPC so everyone instantiates their prefabs
                    //PhotonNetwork.RPC(photonView, "InstantiateObject", PhotonTargets.AllBuffered, false, obj);
					GameEvents.InstantiateGameplayObjects(obj);
                }
            }
            _currentPuzzle = Puzzles[_currentPuzzleID];
        //}

	}

    void OnEnable()
    {        
        GameEvents.OnPlayerActivatedPuzzle += PlayerActivatedPuzzle;
        GameEvents.OnPlayerDeactivatedPuzzle += PlayerDeactivatedPuzzle;
    }

    void OnDisable()
    {        
         GameEvents.OnPlayerActivatedPuzzle -= PlayerActivatedPuzzle;
         GameEvents.OnPlayerDeactivatedPuzzle -= PlayerDeactivatedPuzzle;
    }

    private void PlayerActivatedPuzzle(Civilization civ)
    {
        photonView.RPC("PuzzleActivated", PhotonTargets.All, null);
    }

    private void PlayerDeactivatedPuzzle(Civilization civ)
    {
        photonView.RPC("PuzzleDeactivated", PhotonTargets.All, null);
    }

    [PunRPC]
    void PuzzleActivated()
    {
        Debug.Log("ACTIVATED");
        _currentPuzzle.Activated++;
        if (_currentPuzzle.Activated >= 4)
        {
			_currentPuzzle.Door.Open ();
			GameEvents.PuzzleCompleted();
			_currentPuzzleID++;
			if (_currentPuzzleID < Puzzles.Length)
			{
				_currentPuzzle = Puzzles[_currentPuzzleID];
			}
			else
			{
				GameEvents.GameWon();
			}
        }
    }

    [PunRPC]
    void PuzzleDeactivated()
    {
        Debug.Log("DEACTIVATED");
        _currentPuzzle.Activated--;
		if (_currentPuzzle.Activated < 0)
			_currentPuzzle.Activated = 0;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{

	}

    [PunRPC]
    void InstantiateObject(Objective obj)
    {
        // Make each player Instantiate its own gameplay objects
        GameEvents.InstantiateGameplayObjects(obj);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
